#include <fcntl.h>
#include <stdio.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/shm.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/mman.h>
#include <string.h>
#include <sys/wait.h>


#define MAX_LENGTH 255
#define MUTEXRP 0
#define EMPTYRP 1
#define FULLRP  2
#define MUTEXWP 3
#define EMPTYWP 4
#define FULLWP  5

int WAIT(int sem_des, int num_semaforo){
    struct sembuf operazioni[1] = {{num_semaforo,-1,0}};
    return semop(sem_des,operazioni,1);
}

int SIGNAL(int sem_des, int num_semaforo){
    struct sembuf operazioni[1] = {{num_semaforo,+1,0}};
    return semop(sem_des,operazioni,1);
}

int contrPalindroma(char *stringa, int len){
    int diverso = 0,i=0;

    while(diverso == 0 && i < (int)(len/2)){
        if(stringa[i] != stringa[len-i-1])
            diverso = 1;
        i++;
    }
    return 1-diverso;
}

int reader(int file_input,int size, char *memRP,int semds){

    printf("Sono il processo lettore \n");
    char *input;
    char letto[MAX_LENGTH];
    //mappo il file in memoria
    if( (input = (char *)mmap(NULL,size,PROT_READ|PROT_EXEC,MAP_PRIVATE,file_input,0)) == MAP_FAILED){
        perror("mmap");
        return -1;
    }

    int i=0,fine=0,fineparola=0;

    while(i<size){
        fineparola=0;
        WAIT(semds,EMPTYRP);
        WAIT(semds,MUTEXRP);
        memset(letto,0,sizeof(letto));

        while(fineparola == 0){
            letto[i-fine] = input[i];
            if(input[i]==(int)('\n')){
                fineparola = 1;
                letto[i-fine]='\0';
            }
            i++;
        }
	memset(memRP,0,sizeof(memRP));
        strcpy(memRP,letto);
        fine = i;
        SIGNAL(semds,MUTEXRP);
        SIGNAL(semds,FULLRP);
    }

    //messaggio speciale che indica la fine
    WAIT(semds,EMPTYRP);
    WAIT(semds,MUTEXRP);
    strcpy(memRP,"-1");
    SIGNAL(semds,MUTEXRP);
    SIGNAL(semds,FULLRP);

    //tolgo la mappatura
    munmap(input,size);
    return 0;
}

int writer(int file_output, char *memPW, int semds){

    printf("Sono il processo scrittore\n");
    int Notfinished = 1;
    char scrivi[MAX_LENGTH];
    while(Notfinished == 1){

        WAIT(semds,FULLWP);
        WAIT(semds,MUTEXWP);
        if(strcmp(memPW,"-1") == 0){
            Notfinished = 0;
        }
        else{
	    
	    strcpy(scrivi,memPW);
	    strcat(scrivi,"\n");

	    if(write(file_output,scrivi,strlen(scrivi)) == -1){
                perror("write");
                return -1;
            }
	    
        }
        SIGNAL(semds,MUTEXWP);
        SIGNAL(semds,EMPTYWP);
    }

    return 0;
}

int parent(char* memRP, char *memPW,int size, int semds){

    printf("Sono il processo padre\n");

    char stringa[MAX_LENGTH];
    int palindroma = 0;
    int Notfinished = 1;

    while(Notfinished == 1){

        WAIT(semds,FULLRP);
        WAIT(semds,MUTEXRP);
        strcpy(stringa,memRP);
        SIGNAL(semds,MUTEXRP);
        SIGNAL(semds,EMPTYRP);

        if(strcmp(stringa,"-1")==0){
            Notfinished = 0;
            palindroma = 1;
        }else{
            palindroma = contrPalindroma(stringa,strlen(stringa));
        }

        if(palindroma == 1){
            WAIT(semds,EMPTYWP);
            WAIT(semds,MUTEXWP);
	    memset(memPW,0,sizeof(memPW));
            strcpy(memPW,stringa);
            SIGNAL(semds,MUTEXWP);
            SIGNAL(semds,FULLWP);
        }
    }

    return 0;
}


int main(int argc, char* argv[]){

    int file_input,file_output;
    int shmRP, shmPW;
    int semds;
    void *memRP, *memPW;

    struct stat buffer;

    if(argc < 2 || argc > 3){
        printf("Use %s <file-input> [file-output]\n",argv[0]);
        return -1;
    }
    //apro il file input
    if((file_input=open(argv[1],O_RDONLY)) == -1){
        perror("open");
        return -1;
    }
    //apro il file output
    if(argc == 3){
        if((file_output=open(argv[2],O_CREAT|O_WRONLY|O_TRUNC,0660)) == -1){
            perror("open");
            return -1;
        }
    }else{
        //se non passo il terzo file, qui metto lo standard output
        file_output = 1;
    }

    if( stat(argv[1],&buffer) == -1){
        perror("stat");
        return -1;
    }

    //creo le aree di memoria condivisa

    if((shmPW = shmget(IPC_PRIVATE,MAX_LENGTH,IPC_CREAT|0660)) == -1){
        perror("shmget");
        return -1;
    }

    if((shmRP = shmget(IPC_PRIVATE,MAX_LENGTH,IPC_CREAT|0660)) == -1){
        perror("shmget");
        return -1;
    }

    //annetto le aree di memoria

    if((memRP = shmat(shmRP,NULL,0)) == (void *)-1){
        printf("shmRP errore\n");
        perror("shmat");
        return -1;
    }

    if((memPW = shmat(shmPW,NULL,0)) == (void *)-1){
        perror("shmat");
        return -1;
    }

    //creo i semafori

    if((semds = semget(IPC_PRIVATE,6,IPC_CREAT|IPC_EXCL|0660)) == -1) {
        perror("semget");
        return -1;
    }

    //imposto i valori iniziali dei semafori
    semctl(semds,MUTEXRP,SETVAL,1);
    semctl(semds,EMPTYRP,SETVAL,1);
    semctl(semds,FULLRP,SETVAL, 0);
    semctl(semds,MUTEXWP,SETVAL,1);
    semctl(semds,EMPTYWP,SETVAL,1);
    semctl(semds,FULLWP,SETVAL, 0);

    if(fork() == 0){
        //R
        int return_value = reader(file_input,buffer.st_size,memRP,semds);
        return return_value;
    }else{

        if(fork() == 0){
            //W
            int return_value = writer(file_output,memPW,semds);
            return return_value;
        }else{
            //P
            int return_value = parent(memRP,memPW,buffer.st_size,semds);
            wait(NULL);
            wait(NULL);
            //chiudo i file
            close(file_input);
            if(argc == 3)   close(file_output);

            //libero le aree di memoria
            if(shmctl(shmRP,IPC_RMID,NULL)==-1){
                perror("shmctl");
                return -1;
            }

            if(shmctl(shmPW,IPC_RMID,NULL)==-1){
                perror("shmctl");
                return -1;
            }

            //rimuovo i semafori
            semctl(semds,MUTEXRP,IPC_RMID,0);
            semctl(semds,EMPTYRP,IPC_RMID,0);
            semctl(semds,FULLRP,IPC_RMID, 0);
            semctl(semds,MUTEXWP,IPC_RMID,0);
            semctl(semds,EMPTYWP,IPC_RMID,0);
            semctl(semds,FULLWP,IPC_RMID, 0);

            return 0;
        }
    }

}
