#include <string.h>
#include <sys/ipc.h>
#include <sys/types.h>
#include <sys/msg.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>


#define DEFAULT_NUM 30
#define RAW 1
#define DELTA 2
#define PATH "/proc/stat"
#define MAX_SIZE 255
#define CANC '#'
#define AST '*'
#define UND '_'
#define COL 60

typedef struct{

    long type;
    long user;
    long system;
    long idle;
    float mod_user;
    float mod_kernel;

} message;


int Analyzer(int msgid, int num_sam){

    message miomessaggio;
    long iniz_user,iniz_system,iniz_idle;
    long diff_user,diff_system,diff_idle;

    for(int i=0;i<num_sam;i++){

        memset(&miomessaggio,0,sizeof(miomessaggio));
        if((msgrcv(msgid,&miomessaggio,sizeof(miomessaggio)-sizeof(long),RAW,0)) == -1){
            perror("msgrcv");
            return -1;
        }
        if(i == 0){
            iniz_user = miomessaggio.user;
            iniz_system = miomessaggio.system;
            iniz_idle = miomessaggio.idle;

        }else{
            diff_user = miomessaggio.user - iniz_user;
            diff_system = miomessaggio.system - iniz_system;
            diff_idle = miomessaggio.idle - iniz_idle;

            //calcolo le percentuali
            miomessaggio.type = DELTA;
            miomessaggio.mod_user = ((float)diff_user)/((float)(diff_user+diff_system+diff_idle));
            miomessaggio.mod_kernel = ((float)diff_system)/((float)(diff_user+diff_system+diff_idle));
            //printf("A: %ld %ld %ld\n",diff_user,diff_system,diff_idle);

            if((msgsnd(msgid,&miomessaggio,sizeof(miomessaggio)-sizeof(long),0)) == -1){
            perror("msgsnd");
            return -1;
            }

            iniz_user = miomessaggio.user;
            iniz_system = miomessaggio.system;
            iniz_idle = miomessaggio.idle;


        }



    }
    return 0;
}

int Sampler(int msgid,int num_sam){

    message miomessaggio;
    FILE *file;
    char buffer[MAX_SIZE];
    for(int i=0;i<num_sam;i++){

        memset(buffer,0,sizeof(buffer));
        memset(&miomessaggio,0,sizeof(miomessaggio));

        if((file = fopen(PATH,"r")) == NULL){
            perror("fopen");
            return -1;
        }
        miomessaggio.type = RAW;
        fgets(buffer,sizeof(buffer),file);
        //printf("S: %s\n",buffer);
        strtok(buffer," ");
        miomessaggio.user = atol(strtok(NULL," "));
        strtok(NULL," ");
        miomessaggio.system = atol(strtok(NULL," "));
        miomessaggio.idle = atol(strtok(NULL," "));

        //printf("S: %ld %ld %ld\n",miomessaggio.user,miomessaggio.system,miomessaggio.idle);

        //invio il messaggio
        if((msgsnd(msgid,&miomessaggio,sizeof(miomessaggio)-sizeof(long),0)) == -1){
            perror("msgsnd");
            return -1;
        }

        //chiudo il file
        fclose(file);
        sleep(1);

    }
    return 0;
}

int Plotter(int msgid,int num_sam){

    message miomessaggio;

    for(int i=0;i<num_sam-1;i++){

    memset(&miomessaggio,0,sizeof(miomessaggio));

    if((msgrcv(msgid,&miomessaggio,sizeof(miomessaggio)-sizeof(long),DELTA,0)) == -1){
            perror("msgrcv");
            return -1;
    }

    int num_canc = (int)(miomessaggio.mod_kernel*COL);
    int num_ast = (int)(miomessaggio.mod_user*COL);
    int num_und = COL - num_canc - num_ast;

    printf("\n");
    for(int i=0;i<num_canc;i++){
        printf("%c",CANC);
    }

    for(int i=0;i<num_ast;i++){
        printf("%c",AST);
    }

    for(int i=0;i<num_und;i++){
        printf("%c",UND);
    }

    printf(" s: %.2f%% u: %.2f%%\n",miomessaggio.mod_kernel*100,miomessaggio.mod_user*100);

    }
    return 0;
}




int main(int argc, char * argv[]){

    int num_sam = DEFAULT_NUM,msgid;

    if(argc>2){
        printf("Use %s [number of samples]\n",argv[0]);
        return -1;
    }

    if(argc == 2)   num_sam=atoi(argv[1]);
    if((msgid = msgget(IPC_PRIVATE,IPC_CREAT|IPC_EXCL|0660)) == -1){
        perror("msgget");
        return -1;
    }

    if( fork () == 0){
            //Analizer
            int return_state = Analyzer(msgid,num_sam);
            return return_state;

    }else{

        if(fork() == 0){
            //Plotter
            int return_state = Plotter(msgid,num_sam);
            return return_state;

        }else{
            //Sampler
            Sampler(msgid,num_sam);

        }
    }

    wait(NULL);
    wait(NULL);

    msgctl(msgid,IPC_RMID,NULL);
    return 0;
}
