#include <sys/ipc.h>
#include <sys/msg.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <string.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <dirent.h>
#include <fcntl.h>
#include <sys/mman.h>

#define MAX_SIZE 255

typedef struct {
    long type;
    char comando[MAX_SIZE];
    char param2[MAX_SIZE];
    char param3;
} message;

int sendMessage(int msgds,int type, char* comando, char* param2, char param3){
    message miomessaggio;

    strcpy(miomessaggio.comando, comando);
    strcpy(miomessaggio.param2,param2);
    miomessaggio.param3 = param3;
    miomessaggio.type = (long) type;

    if((msgsnd(msgds,&miomessaggio,sizeof(miomessaggio)-sizeof(long),0))==-1){
        perror("msgsnd");
        return -1;
    }
    return 0;
}

int figlio(int msgds, int num_figlio, char *dir){
    message miomessage;
    DIR *directory;
    struct dirent *voce;

    struct stat buffer;

    if((stat(dir,&buffer))== -1){
        perror("stat");
        return -1;
    }

    if(!S_ISDIR(buffer.st_mode)){
        printf("Errore! l'argomento %d passato non e' una directory\n",num_figlio);
        return -1;
    }

    int hofinito =0;
    while(hofinito == 0){
        if((msgrcv(msgds,&miomessage,sizeof(miomessage)-sizeof(long),(long)num_figlio,0))==-1){
            perror("msgrcv");
            return -1;
        }
        if(strcmp(miomessage.comando,"num-files")==0){

            if( (directory = opendir(dir) )== NULL){
                perror("opendir");
                return -1;
            }
            char pathvoce[2*MAX_SIZE];
            long numero = 0l;
            while(1){
                memset(pathvoce,0,sizeof(pathvoce));
                if((voce = readdir(directory))== NULL){
                    break;
                }
                numero++;
            }

            char stringa[MAX_SIZE];
            memset(stringa,0,sizeof(stringa));
            sprintf(stringa,"%ld file",numero);
            sendMessage(msgds,255,stringa,"0",'0');
            closedir(directory);

        }
        else if(strcmp(miomessage.comando,"total-size")==0){

            if( (directory = opendir(dir) )== NULL){
                perror("opendir");
                return -1;
            }
            char pathvoce[2*MAX_SIZE];
            long dimensione = 0l;
            while(1){
                memset(pathvoce,0,sizeof(pathvoce));
                if((voce = readdir(directory))== NULL){
                    break;
                }
                strcpy(pathvoce,dir);
                strcat(pathvoce,"/");
                strcat(pathvoce,voce->d_name);
                if((stat(pathvoce,&buffer))== -1){
                    perror("stat");
                    return -1;
                }
                dimensione += buffer.st_size;
            }

            char stringa[MAX_SIZE];
            memset(stringa,0,sizeof(stringa));

            sprintf(stringa,"%ld byte",dimensione);
            sendMessage(msgds,255,stringa,"0",'0');
            closedir(directory);
        }
        else if(strcmp(miomessage.comando,"search-char")==0){
            char nome[2*MAX_SIZE];
            char *areaMappata;
            int dimFile,fileds;
            long contatore=0;

            memset(nome,0,sizeof(nome));
            strcpy(nome,dir);
            strcat(nome,"/");
            strcat(nome,miomessage.param2);
            if((fileds =open(nome,O_RDWR,0660))==-1){
                perror("open");
                continue;
            }
            if((stat(nome,&buffer))== -1){
                perror("stat");
                continue;
            }
            dimFile = buffer.st_size;
            if((areaMappata = mmap(NULL,dimFile,PROT_READ,MAP_PRIVATE,fileds,0))==(char*)-1){
                perror("mmap");
                continue;
            }

            for(int i=0;i<dimFile;i++){
                if(areaMappata[i]==miomessage.param3)
                    contatore++;

            }

            if((munmap(areaMappata,dimFile))==-1){
                perror("munmap");
                continue;
            }
            close(fileds);

            char stringa[MAX_SIZE];
            memset(stringa,0,sizeof(stringa));

            sprintf(stringa,"%ld",contatore);
            sendMessage(msgds,255,stringa,"0",'0');

        }
        else if(strcmp(miomessage.comando,"quit\n")==0){
            hofinito=1;
        }
    }
    return 0;
}


int padre(int msgds, int num_figli){
    char stringa[MAX_SIZE];
    char comando[MAX_SIZE];
    char param1;
    char param2[MAX_SIZE],param3;
    message miomessage;
    int i;
    do{
        printf("file-shell2> ");
        fgets(stringa,sizeof(stringa),stdin);
        i=0;
        memset(param2,0,sizeof(param2));
        memset(comando,0,sizeof(comando));
        //controllo il valore passato
        while(stringa[i]!=' '&& i<strlen(stringa)){
            comando[i] = stringa[i];
            i++;
        }
        if(stringa[i]==' '){
            comando[i] = '\0';
        }
        if(strcmp(comando,"num-files")==0){
                //comando 1;
                param1 = stringa[i+1];
                if(stringa[i+2]!='\n'){
                    printf("Comando errato, troppi argomenti!\n");
                }else{
                    if(num_figli<(int)(param1 - '0')){
                        printf("Comando errato, non esiste il figlio %d!\n",(int)(param1 - '0'));
                    }else{
                        //comando 1 corretto
                        sendMessage(msgds,(int)(param1 - '0'),comando,"0",'0');
                        if((msgrcv(msgds,&miomessage,sizeof(miomessage)-sizeof(long),(long)MAX_SIZE,0))==-1){
                            perror("msgrcv");
                            return -1;
                        }
                        printf("%s\n",miomessage.comando);
                    }
                }
        }
        else if(strcmp(comando,"total-size")==0){
                //comando 2
                param1 = stringa[i+1];
                if(stringa[i+2]!='\n'){
                    printf("Comando errato, troppi argomenti!\n");
                }else{
                    if(num_figli<(int)(param1 - '0')){
                        printf("Comando errato, non esiste il figlio %d!\n",(int)(param1 - '0'));
                    }else{
                        //comando 2 corretto
                        sendMessage(msgds,(int)(param1 - '0'),comando,"0",'0');
                        if((msgrcv(msgds,&miomessage,sizeof(miomessage)-sizeof(long),(long)MAX_SIZE,0))==-1){
                            perror("msgrcv");
                            return -1;
                        }
                        printf("%s\n",miomessage.comando);
                    }
                }
        }
        else if(strcmp(comando,"search-char")==0){
                //comando 3
                param1 = stringa[i+1];
                i=i+3;
                int iniz=i;
                while(stringa[i]!=' '&& i<strlen(stringa)){
                    param2[i-iniz] = stringa[i];
                    i++;
                }
                if(stringa[i]==' '){
                    param2[i] = '\0';
                    param3 = stringa[i+1];
                    if(stringa[i+2]!='\n'){
                        printf("Comando errato, troppi argomenti!\n");
                    }else{
                        if(num_figli<(int)(param1 - '0')){
                        printf("Comando errato, non esiste il figlio %d!\n",(int)(param1 - '0'));
                        }else{
                            //comando 3 corretto
                            sendMessage(msgds,(int)(param1 - '0'),comando,param2,param3);
                            if((msgrcv(msgds,&miomessage,sizeof(miomessage)-sizeof(long),(long)MAX_SIZE,0))==-1){
                                perror("msgrcv");
                                return -1;
                            }
                            printf("%s\n",miomessage.comando);
                        }
                    }
                }else{
                    printf("Comando errato, troppi pochi argomenti!\n");
                }


        }
        else if(strcmp(comando,"quit\n")==0){
            for(int i=1;i<=num_figli;i++){
                sendMessage(msgds,i,comando,"0",'0');
            }
            break;
        }else{
            printf("Comando errato!\n");
        }

    }while(strcmp(stringa,"quit\n")!=0);
    return 0;
}

int main(int argc, char* argv[]){

    int msgds;

    if(argc < 2){
        printf("Error! Use %s <dir1> ... <dirN>",argv[0]);
        return -1;
    }

    //creo la coda di messaggi
    msgds = msgget(IPC_PRIVATE,IPC_CREAT|IPC_EXCL|0660);

    for(int i=1;i<argc;i++){

        if(fork() == 0){
            //creo il figlio i-esimo
            int return_status = figlio(msgds,i,argv[i]);
            return 0;
        }
    }
    padre(msgds,argc-1);
    for(int i=1;i<argc;i++){
        wait(NULL);
    }
    //rimuovo la coda di messaggi
    msgctl(msgds,IPC_RMID,NULL);
    return 0;
}
