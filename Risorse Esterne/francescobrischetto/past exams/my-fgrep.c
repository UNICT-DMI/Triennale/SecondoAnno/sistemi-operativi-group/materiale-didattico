// 15 febbraio 2017
#define _GNU_SOURCE


#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <limits.h>
#include <sys/wait.h>


#define MAX_SIZE 255


typedef struct {
    long type;
    char text[MAX_SIZE];
    char file[MAX_SIZE];
} messaggio;

int Filterer(int msgid,int pipeid[2],int command1,int command2,char* stringa,int numFile){
    messaggio miomessaggio;
    int hofinito=0;
    int figliterminati=0;

    FILE *pipescrittura;

    //apro la pipe in scrittura
    if((pipescrittura=fdopen(pipeid[1],"w"))==NULL){
        perror("fdopen");
        return -1;
    }


    while(hofinito == 0){

        char* supporto;

        //leggo il messaggio
        if((msgrcv(msgid,&miomessaggio,sizeof(miomessaggio)-sizeof(long),1,0))== -1){
            perror("msgrcv");
            return -1;
        }
        if(strcmp(miomessaggio.text,"-1") == 0){
            figliterminati++;
        }
        if(figliterminati==numFile){
            hofinito=1;
        }
        //printf("Filt: ho ricevuto %s e devo controllarlo con opz %d %d con %s\n",miomessaggio.text,command1,command2,stringa);
        if(command2 == 1){
            supporto = strcasestr(miomessaggio.text,stringa);
        }else{
            supporto = strstr(miomessaggio.text,stringa);
        }

        if(command1 == 1 && supporto ==NULL && strcmp(miomessaggio.text,"-1")!=0 ){
            //scrivo sulla pipe
            if(fprintf(pipescrittura,"%s: %s",miomessaggio.file,miomessaggio.text) ==-1){
                perror("fprintf");
                return -1;
            }
        }
        if(command1 == 0 && supporto !=NULL && strcmp(miomessaggio.text,"-1")!=0 ){
            //scrivo sulla pipe
            if(fprintf(pipescrittura,"%s: %s",miomessaggio.file,miomessaggio.text) ==-1){
                perror("fprintf");
                return -1;
            }
        }


    }
    //mando il messaggio finale
    if(fprintf(pipescrittura,"-1: -1\n") ==-1){
            perror("fprintf");
            return -1;
    }

    //chiudo la pipe
    if((fclose(pipescrittura))==-1){
        perror(("fclose"));
        return -1;
    }

    return 0;
}

int Reader(int msgid,char *file){

    FILE *miofile;
    messaggio miomessaggio;
    char riga[MAX_SIZE];

    //apro il file
    if((miofile = fopen(file,"r"))== NULL){
        perror("fopen");
        //invio il messaggio finale
        miomessaggio.type =1;
        strcpy(miomessaggio.text,"-1");
        strcpy(miomessaggio.file,file);
        if((msgsnd(msgid,&miomessaggio,sizeof(miomessaggio)-sizeof(long),0))==-1){
            perror("msgsnd");
            return -1;
        }
        return -1;
    }

    //scandisco il file
    while(feof(miofile)!=1){
        //leggo
        fgets(riga,sizeof(riga),miofile);
        //invio il messaggio
        if(strcmp(riga,"\n")!=0){
            miomessaggio.type =1;
            strcpy(miomessaggio.text,riga);
            strcpy(miomessaggio.file,file);
            if((msgsnd(msgid,&miomessaggio,sizeof(miomessaggio)-sizeof(long),0))==-1){
                perror("msgsnd");
                return -1;
            }
        }
    }
    //invio il messaggio finale
    miomessaggio.type =1;
    strcpy(miomessaggio.text,"-1");
    strcpy(miomessaggio.file,file);
    if((msgsnd(msgid,&miomessaggio,sizeof(miomessaggio)-sizeof(long),0))==-1){
        perror("msgsnd");
        return -1;
    }
    //chiudo il file
    if((fclose(miofile))==-1){
        perror(("fclose"));
        return -1;
    }

    return 0;
}

int Father(int pipeid[2],int numFile){

    FILE *pipelettura;
    int hofinito=0;
    char stampare[MAX_SIZE];


    //apro la pipe in lettura
    if((pipelettura=fdopen(pipeid[0],"r"))==NULL){
        perror("fdopen");
        return -1;
    }
    while(hofinito == 0){

        fgets(stampare,sizeof(stampare),pipelettura);
        if(strstr(stampare,"-1") == NULL){
            printf("%s",stampare);
        }else{
            hofinito=1;
        }

    }

    if((fclose(pipelettura))==-1){
        perror(("fclose"));
        return -1;
    }





    return 0;
}



int main(int argc, char *argv[]){

    int msgid, pipeid[2];


    if(argc<3){
        printf("Use: %s [-v] [-i] [word] <file-1> [file-2] ... [file -n] \n",argv[0]);
        return -1;
    }

    //creo le strutture ipc
    if(pipe(pipeid) == -1){
        perror("pipe");
        return -1;
    }
    if((msgid = msgget(IPC_PRIVATE,IPC_CREAT|IPC_EXCL|0660))==-1){
        perror("msgget");
        return -1;
    }

    //controllo quanti file e quante opzioni ci sono
    int command1, command2;
    command1=command2=0;
    int numFile = argc-2;
    if(argc >3 && strcmp(argv[1],"-v") == 0){
        command1=1;
        numFile--;
    }
    else if(argc>3 && strcmp(argv[1],"-i") == 0){
        command2=1;
        numFile--;
    }
    if(argc > 4 && strcmp(argv[2],"-v") == 0 && command1==0 && command2==1){
        numFile--;
        command1=1;
    }
    else if(argc > 4 && strcmp(argv[2],"-i") ==0 && command1==1 && command2==0){
        numFile--;
        command2=1;
    }


    if(fork() == 0){
        //Filterer
        int return_state = Filterer(msgid,pipeid,command1,command2,argv[argc-numFile-1],numFile);
        return return_state;
    }else{
        if(fork() == 0){
            //Padre
            int return_state = Father(pipeid,numFile);
            return return_state;
        }else{
            for(int i=0;i<numFile;i++){
                if(fork() ==0){
                    //Reader file i

                    int return_state = Reader(msgid,argv[argc-numFile+i]);
                    return return_state;
                }else{
                    //Aspetto prima di creare l'altro processo
                    wait(NULL);
                }
            }
            wait(NULL);
            wait(NULL);

        }

    }

    //distruggo le strutture ipc
    if((msgctl(msgid,IPC_RMID,NULL))==-1){
        perror("msgctl");
        return -1;
    }
    return 0;
}
