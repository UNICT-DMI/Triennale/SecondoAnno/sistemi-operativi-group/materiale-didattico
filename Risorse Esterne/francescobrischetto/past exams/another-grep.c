//8 marzo 2017 15:18 fine 16:00
#include <stdio.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/msg.h>
#include <limits.h>
#include <sys/wait.h>

#define MAX_SIZE 255
typedef struct{

    long type;
    char text[MAX_SIZE];

} messaggio;

int Reader(int pipefd[2],char *file){
    FILE *miofile,*miapipe;
    char s[MAX_SIZE];

    if((miofile = fopen(file,"r")) == NULL){
        perror("fopen");
        return -1;
    }

    if((miapipe = fdopen(pipefd[1],"w")) == NULL){
        perror("fdopen");
        return -1;
    }


    while(feof(miofile) != 1){
        memset(s,0,sizeof(s));
        fgets(s,sizeof(s),miofile);
        if(strcmp(s,"\n") != 0)   fputs(s,miapipe);

    }
    fputs("-1",miapipe);

    fclose(miofile);
    fclose(miapipe);
    return 0;
}

int Writer(int msgid){
    messaggio miomessaggio;
    while(1){

        if(msgrcv(msgid,&miomessaggio,sizeof(miomessaggio)-sizeof(long),1,0) == -1){
            perror("msgrcv");
            return -1;
        }
        if(strcmp(miomessaggio.text,"-1") == 0){
            break;
        }
        printf("%s",miomessaggio.text);

    }
    return 0;
}

int Father(int msgid, int pipefd[2], char *parola){
    FILE *miapipe;
    char s[MAX_SIZE];
    messaggio miomessaggio;

    if((miapipe = fdopen(pipefd[0],"r")) == NULL){
        perror("fdopen");
        return -1;
    }

    while(1){
        fgets(s,sizeof(s),miapipe);
        if(strcmp(s,"-1")==0){
            break;
        }
        if(strstr(s,parola) != NULL){
            memset(&miomessaggio,0,sizeof(miomessaggio));
            miomessaggio.type = 1;
            strcpy(miomessaggio.text,s);
            if((msgsnd(msgid,&miomessaggio,sizeof(miomessaggio)-sizeof(long),0)) == -1){
                perror("msgsnd");
                return -1;
            }
        }
    }

    memset(&miomessaggio,0,sizeof(miomessaggio));
    miomessaggio.type = 1;
    strcpy(miomessaggio.text,"-1");
    if((msgsnd(msgid,&miomessaggio,sizeof(miomessaggio)-sizeof(long),0)) == -1){
        perror("msgsnd");
        return -1;
    }
    fclose(miapipe);
    return 0;
}


int main(int argc,char *argv[]){

    int pipefd[2];
    int msgid;

    if(argc != 3){
        printf("Use %s <word> <file>\n",argv[0]);
        return -1;
    }

    if(pipe(pipefd)==-1){
        perror("pipe");
        return -1;
    }

    if((msgid = msgget(IPC_PRIVATE,IPC_CREAT|IPC_EXCL|0660)) == -1){
        perror("msgget");
        return -1;
    }

    if( fork() == 0){
        //Reader
        close(pipefd[0]);
        int return_state = Reader(pipefd,argv[2]);
        return return_state;
    }else{

        if(fork() == 0){
            //Writer
            close(pipefd[0]);
            close(pipefd[1]);
            int return_state = Writer(msgid);
            return return_state;

        }else{
            //Father
            close(pipefd[1]);
            int return_state = Father(msgid,pipefd,argv[1]);

        }
    }
    wait(NULL);
    wait(NULL);
    if(msgctl(msgid,IPC_RMID,NULL) == -1){
        perror("msgctl");
        return -1;
    }



    return 0;
}
