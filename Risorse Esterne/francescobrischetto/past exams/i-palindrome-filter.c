//18 dicembre 2017  17:40
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <sys/ipc.h>
//per le named pipe
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>

#include <sys/wait.h>

#include <sys/mman.h>


#define MAX_SIZE 255


int Reader(int fifoRP,char *inputfile,char *readFile,int dimensione,int fileds){

    if(strcmp(inputfile,"stdin")==0){
        char in[MAX_SIZE];
        do{
            memset(in,0,sizeof(in));
            printf("Inserisci la stringa: ");
            fgets(in,sizeof(in),stdin);
            if(strcmp(in,"!quit\n")!=0){
                write(fifoRP,in,strlen(in));
            }

        }while(strcmp(in,"!quit\n")!=0);

    }else{
        int j=0;
        char stringa[MAX_SIZE];
        for(int i=0;i<dimensione;i++){
            if(readFile[i]=='\n'){
                stringa[j] = '\0';
                //ho letto la stringa, la mando al padre
                if(strcmp(stringa,"")!=0)
                    write(fifoRP,stringa,sizeof(stringa));
                memset(stringa,0,sizeof(stringa));
                j=0;
            }else{
                stringa[j] = readFile[i];
                j++;
            }
        }

    }
    write(fifoRP,"-1",sizeof("-1"));
    return 0;
}

int Writer(int fifoPW){

    char in[MAX_SIZE];

    while(1){
        memset(in,0,sizeof(in));
        read(fifoPW,in,sizeof(in));
        if(strcmp(in,"-1")==0){
            break;
        }
        printf("La stringa %s e' palindroma\n",in);

    }
    return 0;
}

int Father(int fifoRP,int fifoPW,int argc){
    int palindroma,i;
    char stringa[MAX_SIZE];
    memset(stringa,0,sizeof(stringa));
    while(1){
        palindroma=1;
        i=0;
        read(fifoRP,stringa,sizeof(stringa));
        if(strcmp(stringa,"-1")==0){
            break;
        }

        if(argc == 1)   stringa[strlen(stringa)-1]='\0';

        while(palindroma==1&&i<strlen(stringa)/2){
                char app1[2],app2[2];
                memset(app1,0,sizeof(app1));
                memset(app2,0,sizeof(app2));
                app1[0]=stringa[i];
                app1[1]='\0';
                app2[0]=stringa[strlen(stringa)-i-1];
                app2[1]='\0';
                if(strcasecmp(app1,app2) != 0){
                    palindroma=0;
                }
                i++;

        }
        if (argc == 2){   stringa[strlen(stringa)+1]='\0';    stringa[strlen(stringa)] = '\0';}

        if(palindroma==1){
            write(fifoPW,stringa,MAX_SIZE);
        }

    }
    write(fifoPW,"-1",sizeof("-1"));
    return 0;

}


int main(int argc,char *argv[]){

    int fifoRP,fifoPW;
    int fileds;
    char pathRP[MAX_SIZE],pathPW[MAX_SIZE];
    char inputfile[MAX_SIZE];
    int dimensione;


    char DIR[MAX_SIZE];
    if(getcwd(DIR,MAX_SIZE)==NULL){
        perror("getcwd");
        return -1;
    }
    strcat(DIR,"/");

    strcpy(pathRP,DIR);
    strcat(pathRP,"rp");

    strcpy(pathPW,DIR);
    strcat(pathPW,"pw");


    if(argc>2){
        printf("Use: %s <input file>",argv[0]);
        return -1;
    }

    //creo le named pipe
    if((mkfifo(pathRP,0660))==-1){
        perror("mkfifo");
        return -1;
    }

    if((mkfifo(pathPW,0660))==-1){
        perror("mkfifo");
        return -1;
    }

    fifoRP = open(pathRP,O_RDWR);
    fifoPW = open(pathPW,O_RDWR);

    if(fork() == 0) {
        //Reader
        if(argc == 2){

            strcpy(inputfile,argv[1]);
            //mappo il file in memoria
            struct stat buffer;

            if(stat(inputfile,&buffer)==-1){
                perror("stat");
                return -1;
            }

            dimensione = buffer.st_size;
            char *readFile;
            if((fileds = open(inputfile,O_RDWR))== -1){
                perror("open");
                return -1;
            }

            if((readFile = mmap(NULL,dimensione,PROT_READ,MAP_PRIVATE,fileds,0) ) == MAP_FAILED){
                perror("mmap");
                return -1;
            }
            int return_state = Reader(fifoRP,inputfile,readFile,dimensione,fileds);
            //chiudo il file
            close(fileds);

            //faccio unmap
            if(munmap(readFile,dimensione)==-1){
                perror("munmap");
                return -1;
            }

            return return_state;

        }else{
            strcpy(inputfile,"stdin");
            dimensione = -1;
            int return_state = Reader(fifoRP,inputfile,"",-1,-1);
            return return_state;
        }

    }else{

        if(fork() == 0){
            //Writer
            int return_state = Writer(fifoPW);
            return return_state;

        }else{
            //Padre
            int return_state = Father(fifoRP,fifoPW,argc);
        }


    }
    wait(NULL);
    wait(NULL);

    close(fifoPW);
    close(fifoRP);
    //elimino le namedpipe
    if(unlink(pathRP)==-1){
        perror("unlink");
        return -1;
    }

    if(unlink(pathPW)==-1){
        perror("unlink");
        return -1;
    }



    return 0;
}
