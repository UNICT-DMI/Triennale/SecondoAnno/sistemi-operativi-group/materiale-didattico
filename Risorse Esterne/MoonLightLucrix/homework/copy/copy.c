#include<stdio.h>
#include<stdlib.h>
#include<fcntl.h>
#include<unistd.h>
#include<string.h>
#define N 4096

char*getFileName(char*path)
{
	char*fileName=strtok(path,"/");
    if(strtok(NULL,"/")!=NULL)
    {
        while((fileName=strtok(NULL,"/"))!=NULL)
        {
            if(strtok(NULL,"/")==NULL)
            {
                return fileName;
            }
        }
    }
    return fileName;
}

int main(int argc,char**argv)
{
	if(argc<=2)
	{
		printf("usage: %s [source file] [destination directory]",argv[0]);
		exit(1);
	}
	for(int i=1; i<=argc-2; i++)
	{
		int sd, dd, size;
		char buffer[N];
		if((sd=open(argv[i],O_RDONLY))==-1)
		{
			perror("error: can't open file");
			exit(1);
		}
		char fileName[strlen(argv[i])+strlen(argv[argc-1])+1];
		sprintf(fileName,"%s/%s",argv[argc-1],getFileName(argv[i]));
		if((dd=creat(fileName,0644))==-1)
		{
			perror("error: can't create file");
			exit(1);
		}
        
		do{
			if((size=read(sd,buffer,N))==-1)
			{
				perror("error: can't read on file");
				exit(1);
			}
			if((write(dd,buffer,size))==-1)
			{
				perror("error: can't write on new file");
				exit(1);
			}
		}while(N==size);
		close(sd);
		close(dd);
	}
	exit(0);
}
