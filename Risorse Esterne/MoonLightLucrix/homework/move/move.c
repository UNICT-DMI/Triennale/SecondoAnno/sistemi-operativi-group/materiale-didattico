#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<fcntl.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<limits.h>
#include<dirent.h>
#include<libgen.h>
#include<unistd.h>
#include<errno.h>
#define true 1
#define false 0
#define N 4096
typedef char bool;
char program[N];

int dirSec(char*path)
{
	if((access(path,F_OK))==-1)
	{
		//fprintf(stderr,"%s: \"%s\": ",program,path);
		//perror("");
		return -1;
	}
	if((access(path,R_OK))==-1)
	{
		fprintf(stderr,"%s: \"%s\": ",program,path);
		perror("");
		return -1;
	}
	return 0;
}

int move(char*srcPath,char*dstPath)
{
	if((link(srcPath,dstPath))==-1)
	{
		fprintf(stderr,"%s: ",program);
		perror("");
		return -1;
	}
	unlink(srcPath);
	return 0;
}

int copy(char*srcPath,char*dstPath,mode_t mode)
{
	int sd,dd;
	if(((sd=open(srcPath,O_RDONLY))==-1) || ((dd=open(dstPath,O_RDWR | O_CREAT | O_TRUNC,mode))==-1))
	{
		fprintf(stderr,"%s: ",program);
		perror("");
		return -1;
	}
	ssize_t size;
	char*buffer=(char*)malloc(N);
	do{
		if((size=read(sd,buffer,N))==-1)
		{
			fprintf(stderr,"%s: ",program);
			perror("");
			free(buffer);
			return -1;
		}
		if((write(dd,buffer,size))==-1)
		{
			fprintf(stderr,"%s: ",program);
			perror("");
			free(buffer);
			return -1;
		}
	}while(size==N);
	close(sd);
	close(dd);
	if(buffer)
	{
		free(buffer);
	}
	remove(srcPath);
	return 0;
}

int sLnCopy(char*srcPath,char*dstPath,mode_t mode)
{
	ssize_t size;
	char*buffer=(char*)malloc(N);
	do{
		if((size=readlink(srcPath,buffer,N))==-1)
		{	
			fprintf(stderr,"%s: ",program);
			perror("");
			return -1;
		}
		if((symlink(dstPath,buffer))==-1)
		{
			fprintf(stderr,"%s: ",program);
			perror("");
			return -1;
		}
	}while(size==N);
	if(buffer)
	{
		free(buffer);
	}
	unlink(srcPath);
	return 0;
}

int moveDir(char*srcPath,char*dstPath,bool isSameFS)
{
	struct stat statBuff;
	struct dirent*info;
	DIR*folder=opendir(srcPath);
	if(folder)
	{
		char newDstPath[PATH_MAX];
		while((info=readdir(folder)))
		{
			if((strcmp(info->d_name,".")) && (strcmp(info->d_name,"..")))
			{
				char newSrcPath[PATH_MAX];
				sprintf(newSrcPath,"%s/%s",srcPath,info->d_name);
				if((lstat(newSrcPath,&statBuff))==-1)
				{
					fprintf(stderr,"%s: ",program);
					perror("");
					return -1;
				}
				sprintf(newDstPath,"%s/%s",dstPath,info->d_name);
				//printf("srcPath: %s, dstPath: %s, newDstPath: %s\n",srcPath,dstPath,newDstPath);
				//exit(2);
				switch(statBuff.st_mode & S_IFMT)
				{
					case(S_IFREG):
					{
						if(isSameFS)
						{
							if((move(newSrcPath,newDstPath))==-1)
							{
								return -1;
							}
						}
						else
						{
							if((copy(newSrcPath,newDstPath,statBuff.st_mode))==-1)
							{
								return -1;
							}
						}
						break;
					}
					case(S_IFDIR):
					{
						if(((mkdir(newDstPath,statBuff.st_mode))==-1) || ((moveDir(newSrcPath,newDstPath,isSameFS))==-1))
						{
							fprintf(stderr,"%s: ",program);
							perror("");
							return -1;
						}
						break;
					}
					case(S_IFLNK):
					{
						if(isSameFS)
						{
							if((move(newSrcPath,newDstPath))==-1)
							{
								return -1;
							}
						}
						else
						{
							if((sLnCopy(newSrcPath,newDstPath,statBuff.st_mode))==-1)
							{
								return -1;
							}
						}
						break;
					}
				}
			}
		}
		if((rmdir(srcPath))==-1)
		{
			fprintf(stderr,"%s: ",program);
			perror("");
			return -1;
		}
	}
	return 0;
}

int myRename(char*oldName,char*newName,mode_t mode)
{
	switch(mode & S_IFMT)
	{
		case(S_IFREG):
		{
			if((move(oldName,newName))==-1)
			{
				return -1;
			}
			break;
		}
		case(S_IFDIR):
		{
			if((mkdir(newName,mode))==-1)
			{
				fprintf(stderr,"%s: ",program);
				perror("");
				return -1;
			}
			if((moveDir(oldName,newName,true))==-1)
			{
				return -1;
			}
			break;
		}
		case(S_IFLNK):
		{
			if((move(oldName,newName))==-1)
			{
				return -1;
			}
			break;
		}
	}
	return 0;
}

int myMove(char*srcPath,char*dstPath)
{
	struct stat srcStatBuff,dstStatBuff;
	if((lstat(srcPath,&srcStatBuff))==-1)
	{
		fprintf(stderr,"%s: ",program);
		perror("");
		return -1;
	}
	if((lstat(dstPath,&dstStatBuff))==-1)
	{
		if(errno==ENOENT)
		{
			if((myRename(srcPath,dstPath,srcStatBuff.st_mode))==-1)
			{
				return -1;
			}
			return 0;
		}
		else
		{
			fprintf(stderr,"%s: ",program);
			perror("");
			return -1;
		}
	}
	bool isSameFS=false;
	if(srcStatBuff.st_dev==dstStatBuff.st_dev)
	{
		isSameFS=true;
	}
	char newDstPath[PATH_MAX];
	if(dstPath[strlen(dstPath)-1]=='/')
	{
		dstPath[strlen(dstPath)-1]='\0';
	}
	if(srcPath[strlen(srcPath)-1]=='/')
	{
		srcPath[strlen(srcPath)-1]='\0';
	}
	sprintf(newDstPath,"%s/%s",dstPath,basename(srcPath));
	switch(srcStatBuff.st_mode & S_IFMT)
	{
		case(S_IFREG):
		{
			//printf("ciao\n");
			if(isSameFS)
			{
				if((move(srcPath,newDstPath))==-1)
				{
					return -1;
				}
			}
			else
			{
				if((copy(srcPath,newDstPath,srcStatBuff.st_mode))==-1)
				{
					return -1;
				}
				remove(srcPath);
			}
			break;
		}
		case(S_IFDIR):
		{
			//printf("newDstPath: %s\n",newDstPath);
			//exit(2);
			if((mkdir(newDstPath,srcStatBuff.st_mode))==-1)
			{
				fprintf(stderr,"%s: ",program);
				perror("");
				return -1;
			}
			if((moveDir(srcPath,newDstPath,isSameFS))==-1)
			{
				fprintf(stderr,"%s: ",program);
				perror("");
				return -1;
			}
			break;
		}
		case(S_IFLNK):
		{
			if(isSameFS)
			{
				if((move(srcPath,newDstPath))==-1)
				{
					return -1;
				}
			}
			else
			{
				if((sLnCopy(srcPath,newDstPath,srcStatBuff.st_mode))==-1)
				{
					return -1;
				}
			}
			break;
		}
	}
	return 0;
}

void help()
{
	printf("usage: %s source target\n       %s source ... directory\nRename SOURCE to DEST, or move SOURCE(s) to DIRECTORY.\n",program,program);
}

int main(int argc,char**argv)
{
	extern int errno;
	strcpy(program,argv[0]);
	if((argc>=2) && (!strcmp(argv[1],"--help")))
	{
		help();
		exit(EXIT_SUCCESS);
	}
	if(argc<=2)
	{
		printf("usage: %s source target\n       %s source ... directory\n",program,program);
		exit(EXIT_SUCCESS);
	}
	if((dirSec(argv[argc-1]))==-1)
	{
		if(argc!=3)
		{
			fprintf(stderr,"%s: \"%s\": ",program,argv[argc-1]);
			perror("");
			exit(EXIT_FAILURE);
		}
	}
	for(int i=1; i<=argc-2; i++)
	{
		if((dirSec(argv[i]))==-1)
		{
			fprintf(stderr,"%s: \"%s\": ",program,argv[i]);
			perror("");
			continue;
		}
		if((myMove(argv[i],argv[argc-1]))==-1)
		{
			exit(EXIT_FAILURE);
		}
	}
	exit(EXIT_SUCCESS);
}
