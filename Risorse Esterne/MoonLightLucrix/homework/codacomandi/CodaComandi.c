#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<sys/ipc.h>
#include<sys/types.h>
#include<sys/msg.h>
#include<unistd.h>
#include<sys/wait.h>
#include<errno.h>
#define true 1
#define false 0
#define N 4096

typedef struct
{
    long int type;
    char text[N];
} Messaggio;

int inserisci(int msqid)
{
    Messaggio messaggio;
    size_t length;
    while(true)
    {
        printf("Inserisci comando ");
        fgets(messaggio.text,N,stdin);
        if((length=strlen(messaggio.text))==1)
        {
            continue;
        }
        if(messaggio.text[length-1]=='\n')
        {
            messaggio.text[length-1]='\0';
        }
        messaggio.type=1;
        if((msgsnd(msqid,&messaggio,length+1,IPC_NOWAIT))==-1)
        {
            perror("msgsnd");
            return -1;
        }
        if(!strcmp(messaggio.text,"exit"))
        {
            break;
        }
        sleep(1);
    }
    return 0;
}

int esegui(msqid) //processo figlio
{
    Messaggio messaggio;
    size_t length;
    while(true)
    {
        //printf("ciao\n");
        if((msgrcv(msqid,&messaggio,sizeof(messaggio)-sizeof(long),0,0))==-1)
        {
            perror("msgrcv");
            return -1;
        }
        if(!strcmp(messaggio.text,"exit"))
        {
            break;
        }
        pid_t pid=fork();
        if(!pid)
        {
            if((execlp(messaggio.text,messaggio.text,NULL))==-1)
            {
                fprintf(stderr,"error: command not found\n");
                continue;
            }
        }
        else
        {
            wait(NULL);
        }
    }
    msgctl(msqid,IPC_RMID,NULL);
    return 0;
}

int msgQueueInit(char*program)
{
    int msqid;
    key_t chiave=IPC_PRIVATE;
    if((msqid=msgget(chiave,IPC_CREAT | IPC_EXCL | 0660))==-1)
    {
        perror("msgget");
        return -1;
    }
    pid_t pid;
    if((pid=fork())==-1)
    {
        fprintf(stderr,"error: can't create a new process\n");
        return -1;
    }
    if(!pid)
    {
        if((esegui(msqid))==-1)
        {
            perror("esegui");
            exit(2);
        }
        exit(0);
    }
    else
    {
        if((inserisci(msqid))==-1)
        {
            perror("inserisci");
            return -1;
        }
    }
    return 0;
}

int main(int argc,char**argv)
{
    if((msgQueueInit(argv[0]))==-1)
    {
        perror("error: can't start program");
        exit(1);
    }
    exit(0);
}
