#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<fcntl.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<sys/ipc.h>
#include<sys/msg.h>
#include<unistd.h>
#include<sys/wait.h>
#define true 1
#define false 0
#define STDIN 0
#define STDOUT 1
#define STDERR 2
#define N 4096

typedef struct
{
    long int type;
    char text[N];
} Messaggio;

int inserisci(int msqid)
{
    Messaggio messaggio;
    size_t length;
    while(true)
    {
        printf("Insersci comando ");
        fgets(messaggio.text,N,stdin);
        if((length=strlen(messaggio.text))==1)
        {
            continue;
        }
        else if(messaggio.text[length-1]=='\n')
        {
            messaggio.text[length-1]='\0';
        }
        messaggio.type=true;
        if((msgsnd(msqid,&messaggio,length+1,IPC_NOWAIT))==-1)
        {
            perror("error: msgsnd");
            return -1;
        }
        if(!strcmp(messaggio.text,"exit"))
        {
            break;
        }
        if((msgrcv(msqid,&messaggio,sizeof(messaggio)-sizeof(long),0,0))==-1)
        {
            perror("error: msgrcv");
            return -1;
        }
        printf("messaggio: \"%s\"\n",messaggio.text);
        //sleep(1);
    }
    return 0;
}

int esegui(int msqid)
{
    Messaggio messaggio;
    int pipefd[2];
    while(true)
    {
        if((msgrcv(msqid,&messaggio,sizeof(messaggio)-sizeof(long),0,0))==-1)
        {
            perror("error: msgrcv");
            return -1;
        }
        if(!strcmp(messaggio.text,"exit"))
        {
            break;
        }
        if((pipe(pipefd))==-1)
        {
            perror("error: pipe");
            return -1;
        }
        pid_t pid;
        if((pid=fork())==-1)
        {
            perror("error: can't create a new process");
            return -1;
        }
        if(!pid)
        {
            close(pipefd[0]);
            close(STDOUT);
            dup(pipefd[1]);
            close(STDERR);
            dup(pipefd[1]);
            if((execlp(messaggio.text,messaggio.text,NULL))==-1)
            {
                fprintf(stderr,"error: \"%s\" command not found\n",messaggio.text);
                exit(2);
            }
            exit(0);
        }
        else
        {
            close(pipefd[1]);
            ssize_t size;
            if((size=read(pipefd[0],messaggio.text,N))==-1)
            {
                perror("error: can't read file");
                return -1;
            }
            messaggio.text[size-1]='\0';
            messaggio.type=true;
            if((msgsnd(msqid,&messaggio,size+1,0))==-1)
            {
                perror("error: msgsnd");
                return -1;
            }
            close(pipefd[0]);
        }
    }
    msgctl(msqid,IPC_RMID,NULL);
    return 0;
}

int msgQueueInit(char*program)
{
    int msqid;
    mode_t mode=S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH;
    key_t chiave=IPC_PRIVATE;
    if((msqid=msgget(chiave,IPC_CREAT | IPC_EXCL | mode))==-1)
    {
        perror("error: msgget");
        return -1;
    }
    pid_t pid;
    if((pid=fork())==-1)
    {
        perror("error: can't create a new process");
        return -1;
    }
    if(!pid)
    {
        if((esegui(msqid))==-1)
        {
            perror("error: esegui");
            exit(2);
        }
        exit(0);
    }
    else
    {
        if((inserisci(msqid))==-1)
        {
            perror("error: inserisci");
            return -1;
        }
    }
    return 0;
}

int main(int argc,char**argv)
{
    if((msgQueueInit(argv[0]))==-1)
    {
        perror("error: can't start program");
        exit(1);
    }
    exit(0);
}
